package com.codemiro.pingme;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class LambdaPingHandler implements RequestHandler<String, String> {

	private static final Logger LOGGER = LoggerFactory.getLogger(LambdaPingHandler.class);

	//@Autowired
	//private PingController pingController;

	@Override
	@Scheduled(fixedDelay = 2000, initialDelay = 5000)
	public String handleRequest(String input, Context context) {
		LOGGER.info("Input :::: " +input);
		return "Test";
	}
	
}
