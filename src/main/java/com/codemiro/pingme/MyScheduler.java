package com.codemiro.pingme;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;

import com.codemiro.pingme.service.PingService;

public class MyScheduler {
	@Autowired private PingService pingService;
	
	@Value("${schedule.url}")
	String url;

	@Value("${schedule.delay}")
	Long delay;

	@Scheduled(fixedDelay = 2000, initialDelay = 5000)
	public void scheduleFixedRateWithInitialDelayTask() {
		long now = System.currentTimeMillis() / 1000;
		pingService.pingV2(url);
	}

}
