package com.codemiro.pingme.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

@Service
public class PingService {
	public static String PING = "ping ";

	public String pingV2(@PathVariable("address") String address) {
		Process p;
		String s = "";
		StringBuilder sb = new StringBuilder();
		try {
			p = Runtime.getRuntime().exec(PING + address + " -c 5");
			BufferedReader inputStream = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((s = inputStream.readLine()) != null) {
				sb.append(s + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
        return sb.toString();
	}
}
