package com.codemiro.pingme;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PingController {

	public static String PING = "ping ";
	@GetMapping("/ping/v1/{address}")
	public String pingV1(@PathVariable("address") String address) {
		InetAddress inetAddress;
		boolean isReachable;
		try {
			inetAddress = InetAddress.getByName(address);
			 isReachable = inetAddress.isReachable(50000);
		} catch (Exception e) {
			return e.getMessage();
		}
        return "Is the address " + address +" reachable? " + isReachable;
	}
	
	@GetMapping("/ping/v2/{address}")
	public String pingV2(@PathVariable("address") String address) {
		Process p;
		String s = "";
		StringBuilder sb = new StringBuilder();
		try {
			p = Runtime.getRuntime().exec(PING + address + " -c 5");
			BufferedReader inputStream = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((s = inputStream.readLine()) != null) {
				sb.append(s + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
        return sb.toString();
	}
}
